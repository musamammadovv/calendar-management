package calendar.management.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    private static final String EVENT_API_V1 = "/api/v1/events**";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(EVENT_API_V1).hasAnyRole("ADMIN", "USER");
        http.csrf().disable().cors();
        super.configure(http);
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("obama").password("{noop}123").roles("ADMIN")
                .and()
                .withUser("barack").password("{noop}123").roles("USER");
    }
}
