package calendar.management.controller;

import calendar.management.dto.EventRequestDto;
import calendar.management.dto.EventResponseDto;
import calendar.management.dto.EventUpdateDto;
import calendar.management.service.EventService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/api/v1/events")
public class EventController {

    private final EventService eventService;

    @PostMapping
    public ResponseEntity<EventResponseDto> add(@RequestBody EventRequestDto eventRequestDto) {
        log.trace("Add an event request");
        return ResponseEntity.status(HttpStatus.CREATED).body(eventService.add(eventRequestDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventResponseDto> update(@PathVariable Long id, @RequestBody EventUpdateDto eventUpdateDto) {
        log.trace("Update an event request with id: {}", id);
        return ResponseEntity.ok(eventService.update(id, eventUpdateDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Delete an event request with id: {}", id);
        eventService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping
    public ResponseEntity<List<EventResponseDto>> search(
            @RequestParam(value = "startDate")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam(value = "endDate")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate){
        log.trace("Search events request between interval: {} - {}", startDate, endDate);
        return ResponseEntity.ok(eventService.find(startDate,endDate));
    }

    @GetMapping("/{date}")
    public ResponseEntity<List<EventResponseDto>> search(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date){
        log.trace("Search events request on date: {}", date);
        return ResponseEntity.ok(eventService.find(date));
    }
}
