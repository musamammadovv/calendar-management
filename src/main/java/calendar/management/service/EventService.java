package calendar.management.service;

import calendar.management.dto.EventRequestDto;
import calendar.management.dto.EventResponseDto;
import calendar.management.dto.EventUpdateDto;
import java.time.LocalDate;
import java.util.List;

public interface EventService {
    EventResponseDto add(EventRequestDto eventRequestDto);

    void delete(Long id);

    EventResponseDto update(Long id, EventUpdateDto eventUpdateDto);

    List<EventResponseDto> find(LocalDate date);

    List<EventResponseDto> find(LocalDate startDate, LocalDate endDate);
}
