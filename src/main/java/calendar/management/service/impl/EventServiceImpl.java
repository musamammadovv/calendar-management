package calendar.management.service.impl;

import calendar.management.dto.EventRequestDto;
import calendar.management.dto.EventResponseDto;
import calendar.management.dto.EventUpdateDto;
import calendar.management.exception.EventNotFoundException;
import calendar.management.exception.UserCanNotHandleEventWhichWasAddedByAdminException;
import calendar.management.model.Event;
import calendar.management.repository.EventRepository;
import calendar.management.service.EventService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    private final ModelMapper modelMapper;

    public EventResponseDto add(EventRequestDto eventRequestDto) {
        Event event = modelMapper.map(eventRequestDto, Event.class);
        event.setAuthorRole(checkIfPrincipalRoleIsAdmin() ? "ROLE_ADMIN" : "ROLE_USER");
        event.setRead(false);
        return modelMapper.map(eventRepository.save(event), EventResponseDto.class);
    }

    public void delete(Long id) {
        Event event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
        checkIfUserAttendsToHandleAdminData(event);
        eventRepository.delete(event);
    }

    public EventResponseDto update(Long id, EventUpdateDto eventUpdateDto) {
        Event event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
        checkIfUserAttendsToHandleAdminData(event);
        modelMapper.map(eventUpdateDto, event);
        return modelMapper.map(eventRepository.save(event), EventResponseDto.class);
    }

    @Transactional
    public List<EventResponseDto> find(LocalDate date) {
        List<Event> events = eventRepository.findByDate(date);
        if (checkIfLoggedPrincipalIsUser())
            for (Event event : events) {
                event.setRead(true);
            }
        return events.stream().map(event -> modelMapper.map(event, EventResponseDto.class)).collect(Collectors.toList());
    }

    @Transactional
    public List<EventResponseDto> find(LocalDate startDate, LocalDate endDate) {
        List<Event> events = eventRepository.findByDateBetween(startDate, endDate);
        if (checkIfLoggedPrincipalIsUser())
            for (Event event : events) {
                event.setRead(true);
            }
        return events.stream().map(event -> modelMapper.map(event, EventResponseDto.class)).collect(Collectors.toList());
    }

    private boolean checkIfLoggedPrincipalIsUser() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().
                stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER"));
    }

    private void checkIfUserAttendsToHandleAdminData(Event event) {
        if (event.getAuthorRole().equals("ROLE_ADMIN") && !checkIfPrincipalRoleIsAdmin())
            throw new UserCanNotHandleEventWhichWasAddedByAdminException();
    }

    private Boolean checkIfPrincipalRoleIsAdmin(){
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().
                stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"));
    }
}
