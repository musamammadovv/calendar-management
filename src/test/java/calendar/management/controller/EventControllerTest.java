package calendar.management.controller;

import calendar.management.dto.EventRequestDto;
import calendar.management.dto.EventResponseDto;
import calendar.management.dto.EventUpdateDto;
import calendar.management.service.EventService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(EventController.class)
class EventControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final LocalDate DUMMY_DATE = LocalDate.parse("1997-12-20");
    private static final String DUMMY_TYPE = "Holiday";
    private static final String BASE_URL = "/api/v1/events";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EventService eventService;

    private EventResponseDto eventResponseDto;
    private EventRequestDto eventRequestDto;
    private EventUpdateDto eventUpdateDto;

    @BeforeEach
    void setUp() {
        eventResponseDto = EventResponseDto
                .builder()
                .date(DUMMY_DATE)
                .type(DUMMY_TYPE)
                .build();

        eventRequestDto = EventRequestDto
                .builder()
                .date(DUMMY_DATE)
                .type(DUMMY_TYPE)
                .build();

        eventUpdateDto = EventUpdateDto
                .builder()
                .date(DUMMY_DATE)
                .type(DUMMY_TYPE)
                .build();
    }

    @Test
    void givenValidInputWhenGetThenOk() throws Exception {
        //Arrange
        when(eventService.find(DUMMY_DATE)).thenReturn(List.of(eventResponseDto));

        //Act
        mockMvc.perform(get(BASE_URL + "/" + DUMMY_DATE)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void givenInValidDateWhenGetThenException() throws Exception {
        //Arrange
        String date = "DUMMY_DATE";

        //Act
        mockMvc.perform(get(BASE_URL + "/" + date)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(eventService.add(eventRequestDto)).thenReturn(eventResponseDto);

        //Act
        mockMvc.perform(post(BASE_URL)
                .content(objectMapper.writeValueAsString(eventRequestDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        //verify
        verify(eventService, times(1)).add(eventRequestDto);
    }

    @Test
    void givenValidInputWhenUpdateThenReturnOk() throws Exception {
        //Arrange
        when(eventService.update(DUMMY_ID, eventUpdateDto)).thenReturn(eventResponseDto);

        //Act
        mockMvc.perform(put(BASE_URL + "/" + DUMMY_ID)
                .content(objectMapper.writeValueAsString(eventRequestDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //verify
        verify(eventService, times(1)).update(DUMMY_ID, eventUpdateDto);
    }

    @Test
    void givenValidIdWhenDeleteThenNoContent() throws Exception {
        //Action
        ResultActions actions = mockMvc.perform(delete(BASE_URL + "/" + DUMMY_ID)
                .contentType(MediaType.APPLICATION_JSON));

        //Assert
        verify(eventService, times(1)).delete(DUMMY_ID);
        actions.andExpect(status().isNoContent());
    }

    @Test
    void givenInValidIdWhenDeleteThenException() throws Exception {
        //Arrange
        String id = "id";

        //Action
        ResultActions actions = mockMvc.perform(delete(BASE_URL + "/" + id)
                .contentType(MediaType.APPLICATION_JSON));

        //Assert
        actions.andExpect(status().isBadRequest());
    }
}